"use strict";
import { hello } from "../src/hello-world";
import { expect } from "chai";
import "mocha";

// Define a new test group called 'Hello function'
describe("Hello function", (): void => {

	// Define a new test for the group
	it("should return hello world", (): void => {
		const result = hello();
		expect(result).to.equal("Hello World!");
	});
});